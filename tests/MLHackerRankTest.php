<?php 
require_once '../SubMatrixMedian.php';
require_once '../Matrix.php';
use PHPUnit\Framework\TestCase;
use SubMatrixMedianTestMl\Matrix;
use SubMatrixMedianTestMl\SubMatrixMedian;

class MLHackerRankTest extends TestCase
{
	protected $subMatrixMedian;

	public function setUp(){
		$matrix = new Matrix(4,4);
		$matrix->set(1,1,1);
		$matrix->set(1,2,3);
		$matrix->set(1,3,2);
		$matrix->set(1,4,4);
		$matrix->set(2,1,8);
		$matrix->set(2,2,1);
		$matrix->set(2,3,2);
		$matrix->set(2,4,9);
		$matrix->set(3,1,1);
		$matrix->set(3,2,1);
		$matrix->set(3,3,2);
		$matrix->set(3,4,2);
		$matrix->set(4,1,7);
		$matrix->set(4,2,5);
		$matrix->set(4,3,3);
		$matrix->set(4,4,6);

		$this->subMatrixMedian = new SubMatrixMedian($matrix);
	}
	
	public function testSampleInputQuery1()
	{
		$this->assertEquals(2,$this->subMatrixMedian->query(1,1,2,2));
	}

	public function testSampleInputQuery2()
	{
		$this->assertEquals(2,$this->subMatrixMedian->query(1,2,3,4));
	}

	public function testSampleInputQuery3()
	{
		$this->assertEquals(6,$this->subMatrixMedian->query(4,4,4,4));
	}
}
?>