<?php 
require_once '../SubMatrixMedian.php';
require_once '../Matrix.php';
use PHPUnit\Framework\TestCase;
use SubMatrixMedianTestMl\Matrix;
use SubMatrixMedianTestMl\SubMatrixMedian;

class SubMatrixMedianTest extends TestCase
{
	protected $subMatrixMedian;

	public function setUp(){
		$matrix = new Matrix(4,4);
		$matrix->set(1,1,1);
		$matrix->set(1,2,3);
		$matrix->set(1,3,2);
		$matrix->set(1,4,4);
		$matrix->set(2,1,8);
		$matrix->set(2,2,1);
		$matrix->set(2,3,2);
		$matrix->set(2,4,9);
		$matrix->set(3,1,1);
		$matrix->set(3,2,1);
		$matrix->set(3,3,2);
		$matrix->set(3,4,2);
		$matrix->set(4,1,7);
		$matrix->set(4,2,5);
		$matrix->set(4,3,3);
		$matrix->set(4,4,6);

		$this->subMatrixMedian = new SubMatrixMedian($matrix);
	}

	public function testSubMatrixMinRowZero()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(0,3,2,2);
	}

	public function testSubMatrixMaxRowZero()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,0,2,2);
	}

	public function testSubMatrixRowLimitsOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,5,2,2);
	}

	public function testSubMatrixMinRowGreaterThanMaxRow()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(3,2,2,2);
	}

	public function testSubMatrixMinColZero()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,3,0,2);
	}

	public function testSubMatrixMaxColZero()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,3,2,0);
	}

	public function testSubMatrixColLimitsOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,3,1,5);
	}

	public function testSubMatrixMinColGreaterThanMaxRow()
	{
		$this->expectException(Exception::class);
		$this->subMatrixMedian->query(1,3,3,2);
	}

}
?>