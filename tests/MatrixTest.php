<?php 
require_once '../Matrix.php';
use SubMatrixMedianTestMl\Matrix;
use PHPUnit\Framework\TestCase;

class MatrixTest extends TestCase
{
	protected $matrix;

	public function setUp(){
		$this->matrix = new Matrix(3,3);
		$this->matrix->set(1,1,1);
		$this->matrix->set(1,2,2);
		$this->matrix->set(1,3,3);
		$this->matrix->set(2,1,4);
		$this->matrix->set(2,2,5);
		$this->matrix->set(2,3,6);
		$this->matrix->set(3,1,7);
		$this->matrix->set(3,2,8);
		$this->matrix->set(3,3,9);	
	}

	public function testNewMatrixZeroRows(){
		$this->expectException(Exception::class);
		$matrix = new Matrix(0,0);
	}

	public function testInitializedMatrix(){
		$matrix = new Matrix(2,2);
		$this->assertEquals(0, $matrix->get(1,1));
		$this->assertEquals(0, $matrix->get(1,2));
		$this->assertEquals(0, $matrix->get(2,1));
		$this->assertEquals(0, $matrix->get(2,2));
	}

	public function testGetElementRowOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->get(4,1);
	}

	public function testGetElementColumnOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->get(3,4);
	}

	public function testGetElementColumnAndRowOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->get(4,4);
	}


	public function testUpdateElement()
	{
		$this->matrix->set(1,3,10);
		$this->assertEquals(10, $this->matrix->get(1,3));
	}

	public function testUpdateElementRowOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->set(4,1,10);
	}

	public function testUpdateElementColumnOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->set(3,4,10);
	}

	public function testUpdateElementColumnAndRowOutOfBounds()
	{
		$this->expectException(Exception::class);
		$this->matrix->set(4,4,10);
	}
}

?>