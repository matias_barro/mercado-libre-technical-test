<?php 
namespace SubMatrixMedianTestMl;
use Exception;

class Matrix
{
	private $values;
	private $rows;
	private $columns;
	
	function __construct($rows,$columns)
	{
		if($rows < 1 || $columns < 1){
			throw new Exception("Rows and Columns values must be greater than one", 1);
		}

		$this->rows = $rows;
		$this->columns = $columns;
		$this->initialize($rows,$columns);
	}

	private function initialize($rows,$columns){
		$this->values = array();
		for($r = 0; $r < $rows; $r++){
			$colVals = array();
			for($c = 0; $c < $columns; $c++){
				$colVals[$c] = 0;
			}
			$this->values[$r] = $colVals;
		} 
	}

	public function getRows(){
		return $this->rows;
	}

	public function getColumns(){
		return $this->columns;
	}

	public function size(){
		return $this->rows * $this->columns;
	}

	public function get($r,$c){
		$this->checkBounds($r,$c);
		return $this->values[$r - 1][$c - 1];
	}

	public function set($r,$c,$val){
		$this->checkBounds($r,$c);
		return $this->values[$r - 1][$c - 1] = $val;
	}

	private function checkBounds($r,$c){
		if($r > $this->rows || $c > $this->columns){
			throw new Exception("Out of Bounds", 1);
		}	
	}

}

?>