<?php 
namespace SubMatrixMedianTestMl;
use Exception;

class SubMatrixMedian 
{
	private $matrix;

	function __construct($matrix){
		$this->matrix = $matrix;
	}

	public function query($r1,$c1,$r2,$c2){
		//get sub matrix
		$subMatrix = $this->subMatrix($r1,$c1,$r2,$c2);
		return $this->calculateMedian($subMatrix);
	}

	
	private function subMatrix($r1,$c1,$r2,$c2){
		if(!$this->checkSubMatrixBoundValues($r1,$r2,$this->matrix->getRows())){
			throw new Exception("Sub Matrix row limits out of bounds", 1);
		}

		if(!$this->checkSubMatrixBoundValues($c1,$c2,$this->matrix->getColumns())){
			throw new Exception("Sub Matrix colums limits out of bounds", 1);
		}

		$subMatrixRows = ($r2 - $r1) + 1;
		$subMatrixCols = ($c2 - $c1) + 1;
		$subMatrix = new Matrix($subMatrixRows,$subMatrixCols); 
		$subMatrixRowIdx = 1;
		$subMatrixColIdx = 1;

		for ($rowIdx=$r1; $rowIdx <= $r2; $rowIdx++) { 
			for($colIdx = $c1; $colIdx <= $c2; $colIdx++){
				$subMatrix->set($subMatrixRowIdx, $subMatrixColIdx,$this->matrix->get($rowIdx,$colIdx));
				$subMatrixColIdx++;	
			}
			$subMatrixRowIdx++;
			$subMatrixColIdx = 1;
		}

		return $subMatrix;

	}

	private function checkSubMatrixBoundValues($minBound,$maxBound, $bound){
		return ($minBound >=1 && 
				$maxBound >= 1 && 
				$minBound <= $bound && 
				$maxBound <= $bound && 
				$minBound <= $maxBound);
	}

	
	private function calculateMedian($subMatrix){
		//get sorted list of elements 
		$subMatrixSortedElements = $this->subMatrixSortedElements($subMatrix);
		$elementsCount = count($subMatrixSortedElements);
		//check if elements count is even
		if(count($subMatrixSortedElements) % 2 == 0){
			//even count case: median is the average of the two middle numbers
			$middleIdx = ($elementsCount / 2) - 1;
			$firstMiddleElement = $subMatrixSortedElements[$middleIdx];
			$secondMiddleElement = $subMatrixSortedElements[$middleIdx + 1];
			return floor(($firstMiddleElement + $secondMiddleElement) / 2);
		}else{
			//odd count case: median is the middle element of the sorted list
			$middleIdx = floor($elementsCount / 2);
			return $subMatrixSortedElements[$middleIdx];
		}
	}

	private function subMatrixSortedElements($subMatrix){
		$elements = array();
		$elementIdx = 0;

		for($r = 1; $r <= $subMatrix->getRows(); $r++){
			for ($c=1; $c <= $subMatrix->getColumns(); $c++) { 
				$elements[$elementIdx] = $subMatrix->get($r,$c);
				$elementIdx++;
			}
		}

		//sort
		sort($elements);
		return $elements;
	}
}
?>